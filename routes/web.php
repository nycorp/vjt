<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::prefix('/')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
});


Route::get('/post-detail/{post}', 'PostController@show')->name('post.detail');


Route::get('/discovery', function () {
    return view('pages.discovery');
})->name('discovery');
Route::get('/about-us', function () {
    return view('pages.about-us');
})->name('about.us');
Route::get('/contact-us', function () {
    return view('pages.contact-us');
})->name('contact.us');
