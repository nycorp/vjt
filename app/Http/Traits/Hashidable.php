<?php
/**
 * Created by PhpStorm.
 * User: yann-yvan
 * Date: 16/11/18
 * Time: 12:49
 */

namespace App\Http\Traits;

trait Hashidable
{
    public function getRouteKey()
    {
        return \Vinkla\Hashids\Facades\Hashids::connection(get_called_class())->encode($this->getKey());
    }
}