<?php

namespace App;

use App\Http\Traits\Hashidable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $files
 * @property mixed $likes
 * @property mixed $comments
 * @property mixed $category
 */
class Post extends Model
{

    use Hashidable;

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
