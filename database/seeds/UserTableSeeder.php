<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        \App\User::create([
            'name' => 'Admin',
            'email' => 'yann39@live.com',
            'phone' => '695499969',
            'password' => bcrypt("password"),
            'role_id' => \App\Role::where('label', 'Admin')->first()->id
        ]);
    }
}
