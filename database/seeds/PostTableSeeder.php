<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Post::class, 25)->create()->each(function ($post) {
            factory(App\Comment::class, random_int(5, 20))->create();
            factory(App\File::class, random_int(2, 5))->create();
        });
    }
}
