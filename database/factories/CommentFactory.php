<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    $comments = ['Où sont les bikinis?', '😊😊😊on vous attend avec votre maillot c\'est le moment 😎😎',
        'wandayances', 'Cadre sécurisé pour les enfants,avec un maître nageur', '
Tout le monde à la pêche 🐋🐋🐋🐋🐋🐋🐚🐠🐠!tellement d\'activités à faire qui nous éloigne de notre quotidien ☺️☺️☺️'];
    return [
        'message' => $comments[random_int(0, count($comments) - 1)],
        'post_id' => \App\Post::where('id', random_int(1, 25))->first()->id,
        'user_id' => \App\User::where('id', 1)->first()->id,
    ];
});
