<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => 'Post-' . random_int(1, 100000),
        'description' => 'Vous pouvez aussi contempler ce magnifique coucher de soleil à kribi ,quel beau cadre . écrivez nous et nous vous emmènerons dans ce magnifique lieu #we love Africa🎐🌍',
        'user_id' => \App\User::where('id', 1)->first()->id,
        'category_id' => \App\Category::where('id', random_int(1, 2))->first()->id,
    ];
});
