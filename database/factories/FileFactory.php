<?php

use Faker\Generator as Faker;

$factory->define(App\File::class, function (Faker $faker) {
    $files = [
        ['picture', 'posts/44944507_1680257308753198_1505198046529454080_n.jpg'],
        ['picture', 'posts/44974708_1680262725419323_3233147953321345024_n.jpg'],
        ['url', 'https://www.youtube.com/watch?v=pvmNeNd5SYk&start_radio=1&list=RDMMpvmNeNd5SYk'],
    ];

    $file = $files[random_int(0, count($files) - 1)];
    return [
        'type' => $file[0],
        'path' => $file[1],
        'post_id' => \App\Post::where('id', random_int(1, 25))->first()->id,
    ];
});
