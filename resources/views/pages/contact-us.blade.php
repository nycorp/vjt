@extends('layouts.app')
@section('body-class')
    <body class="page contact-us">
    @endsection
    @section('content')
        <div class="div-box">
            <div class="banner-subpage">
                <figure><img src="{{asset('assets/images/background/bg-banner-3.jpg')}}" alt="bg-banner"/></figure>
                <div class="banner-subpage-content">
                    <h2>Contactez nous</h2>
                    <div class="desc">
                        <p>Accueil</p>
                        <p>Contact</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="div-box get-in-touch mb">
            <div class="container">
                <div class="get-in-touch text-center">
                    <h2 class="mb-45">Vous avez une question</h2>
                    <p class="mb-45"><i>Please don’t lorem ipsum dolor sit amet, tellus sit, convallis wisi id magna
                            faucibus lobortis tempus quisque bibendum suspendisse. <br/>I’ll try sapien lorem, lacus
                            dictumst quis anim!</i></p>
                </div>
                <form class="get-in-touch-form">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                <input type="text" placeholder="Name"/>
                            </p>
                            <p>
                                <input type="text" placeholder="Email"/>
                            </p>
                            <p>
                                <input type="text" placeholder="Website"/>
                            </p>
                            <p>
                                <input type="text" placeholder="Subject"/>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <textarea cols="30" rows="14" placeholder="Message"></textarea>
                            </p>
                        </div>
                    </div>
                    <p class="text-right"><a href="#" class="btn btn-17">post</a></p>
                </form>
            </div>
        </div>



@endsection