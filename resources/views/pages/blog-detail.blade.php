@extends('layouts.app')
@section('body-class')
    <body class="blog blog-detail">
    @endsection
    @section('content')
        <div class="div-box">
            <div class="banner-subpage">
                <figure><img src="{{asset()}}" alt="bg-banner"/></figure>
                <div class="banner-subpage-content">
                    <h2>{{$post->title}}</h2>
                    <div class="desc">
                        <p>Home </p>
                        <p>Blog Detail</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="div-box mb mt">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="blog-wrap">
                            <div class="blog-inner blog-style-grid blog-paging-all">
                                <article class="post">
                                    <div class="post-item">
                                        <div class="entry-wrap">
                                            <div class="entry-thumbnail-wrap">
                                                <div data-number="1" data-margin="0" data-loop="yes"
                                                     data-navcontrol="yes" class="begreen-owl-carousel">
                                                    <div><img src="{{asset('assets/images/demo/blog-list-3.jpg')}}"
                                                              alt="blog" width="800"
                                                              height="500" class="img-responsive"/></div>
                                                    <div><img src="{{asset('assets/images/demo/blog-list-6.jpg')}}"
                                                              alt="blog" width="800"
                                                              height="500" class="img-responsive"/></div>
                                                    <div><img src="{{asset('assets/images/demo/blog-list-4.jpg')}}"
                                                              alt="blog" width="800"
                                                              height="500" class="img-responsive"/></div>
                                                </div>
                                            </div>
                                            <div class="entry-content-wrap">
                                                <div class="entry-detail">
                                                    <h3 class="entry-title">
                                                        <a href="blog-detail.html">{{$post->title}}</a></h3>
                                                    <div class="entry-post-meta-wrap">
                                                        <ul class="entry-meta">
                                                            <li class="entry-meta-author">
                                                                <i class="fa fa-pencil-square-o p-color"></i>
                                                                <a href="#">{{$post->user->name}}</a>
                                                            </li>
                                                            <li class="entry-meta-date">
                                                                <i class="fa fa-clock-o p-color"></i>
                                                                <a href="#">{{$post->updated_at->format('d M Y')}}</a>
                                                            </li>
                                                            <li class="entry-meta-category">
                                                                <i class="fa fa-folder-open p-color"></i>
                                                                <a href="#">Planter</a>, <a href="#">Tree</a></li>
                                                            <li class="entry-meta-comment">
                                                                <a href="#">
                                                                    <i class="fa fa-comments-o p-color"></i>{{$post->comments->count()}}
                                                                    Commentaires</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="entry-excerpt">
                                                        {!! html_entity_decode($post->description) !!}
                                                    </div>
                                                    <div class="mb-20"><img
                                                                src="{{asset('assets/images/demo/blog-list-5.jpg')}}"
                                                                alt="blog"
                                                                width="800" height="500"
                                                                class="img-responsive"/></div>
                                                    <div class="entry-excerpt">
                                                        <p>Sed ut perspiciatis unde omnis iste natus error sit
                                                            voluptatem accusantium doloremque laudantium, totam rem
                                                            aperiam, eaque ipsa quae ab illo inventore veritatis et
                                                            quasi architecto beatae vitae dicta sunt explicabo.</p>
                                                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                                                            odit aut fugit, sed quia consequuntur magni dolores eos qui
                                                            ratione voluptatem sequi nesciunt. Neque porro quisquam est,
                                                            qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
                                                            velit, sed quia non numquam eius modi tempora incidunt ut
                                                            labore et dolore magnam aliquam quaerat voluptatem. Ut enim
                                                            ad minima veniam, quis nostrum exercitationem ullam corporis
                                                            suscipit laboriosam, nisi ut aliquid ex ea commodi
                                                            consequatur? Quis autem vel eum iure reprehenderit qui in ea
                                                            voluptate velit esse quam nihil molestiae consequatur, vel
                                                            illum qui dolorem eum fugiat quo voluptas nulla
                                                            pariatur.</p>
                                                    </div>
                                                    <div class="entry-meta-tag-list">
                                                        <div class="entry-meta-tag">
                                                            <label><i class="fa fa-tags"></i>Tags :</label><a href="#">Plant
                                                                Care</a><a href="#">Plant Of The Month</a>
                                                        </div>
                                                        <div class="entry-meta-tag-right">
                                                            <div class="social-share-wrap">
                                                                <label><i class="fa fa-share-alt"></i>Partager:</label>
                                                                <ul class="social-share">
                                                                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i
                                                                                    class="fa fa-google-plus"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-tumblr"></i></a>
                                                                    </li>
                                                                    <li><a href="#"><i class="fa fa-pinterest"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="admin-deatil text-center">
                            <figure><img src="{{asset('assets/images/demo/avartar-2.jpg')}}" alt="avartar"/></figure>
                            <h2 class="mb-20"><a href="#">{{$post->user->name}}</a></h2>
                            <p>Share a little biographical information to fill out your profile. This may be shown
                                publicly.</p>
                        </div>
                        <div id="comments" class="entry-comments">
                            <div class="entry-comments-form">
                                <div id="respond-wrap">
                                    <div id="respond" class="comment-respond">
                                        <h3 id="reply-title" class="comment-reply-title"><span>Leave your thought</span>
                                        </h3>
                                        <form id="commentform" class="comment-form">
                                            <div class="comment-fields-wrap">
                                                <div class="comment-fields-inner clearfix">
                                                    <div class="row">
                                                        <div class="yolo-row">
                                                            <p class="comment-form-author yolo-sm-6 mb-20">
                                                                <input id="author" placeholder="Enter Your Name*"
                                                                       type="text" class="form-control"/>
                                                            </p>
                                                            <p class="comment-form-email yolo-sm-6 mb-20">
                                                                <input id="email" name="email"
                                                                       placeholder="Enter Your Email*" type="text"
                                                                       class="form-control"/>
                                                            </p>
                                                            <div class="yolo-sm-12 mb-20">
                                                                <p class="comment-form-comment">
                                                                    <textarea id="comment"
                                                                              placeholder="Enter Your Comment"
                                                                              name="comment"
                                                                              class="form-control"></textarea>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <p class="form-submit">
                                                            <input id="submit" name="submit" value="Post Comments"
                                                                   type="submit" class="submit"/>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('component.sidebar.content')
                </div>
            </div>
        </div>

@endsection