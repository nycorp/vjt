@extends('layouts.app')
@section('body-class')
    <body class="why-choose-us">
    @endsection
    @section('content')
        <div class="div-box">
            <div class="banner-subpage">
                <figure><img src="{{asset('assets/images/background/bg-banner-3.jpg')}}" alt="bg-banner"/></figure>
                <div class="banner-subpage-content">
                    <h2>Pourquoi nous choisir</h2>
                    <div class="desc">
                        <p>Accueil</p>
                        <p>Qui nous sommes</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="div-box mb mt">
            <div class="container">
                <h2 class="title-style title-style-1 mb-45 text-center"><span class="title-left">Nos </span><span
                            class="title-right">Services</span></h2>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="style_4 icon-box-shortcode-wrap">
                            <div class="icon-box-container">
                                <div class="icon-wrap">
                                    <div class="icon-main bg-base"><span class="fa fa-truck color-fff"></span></div>
                                </div>
                                <div class="icon-content">
                                    <div class="icon-title">On vous emmenent</div>
                                    <p class="icon-description">Lorem ipsum dolor sit amet, morbi justo sit lacinia
                                        vestibulum.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="style_4 icon-box-shortcode-wrap">
                            <div class="icon-box-container">
                                <div class="icon-wrap">
                                    <div class="icon-main bg-base"><span class="fa fa-diamond color-fff"></span></div>
                                </div>
                                <div class="icon-content">
                                    <div class="icon-title"> Vous serez toujours satisfait</div>
                                    <p class="icon-description">Lorem ipsum dolor sit amet, morbi justo sit lacinia
                                        vestibulum.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="div-box mb">
            <div class="row mg-0 equal-container">
                <div class="col-md-6 col-sm-6 pd-0 equal-elem slider-text-left"></div>
                <div class="col-md-6 col-sm-6 pd-0 bg-1f2a37 equal-elem">
                    <div data-number="1" data-margin="0" data-loop="yes" data-navcontrol="no" data-autoplay="yes"
                         data-dots="yes" class="slider-text style_1 begreen-owl-carousel">
                        <div class="slider-item">
                            <h2 class="number">1</h2><a>
                                <h1 class="title">Qui nous sommes?</h1>
                                <div class="desc">We are porttitor amet eleifend sed “Id lorem placerat nulla amet
                                    vivamus dolor” Imperdiet et, tempor aliquam aliquet, magna luctus volutpat nisl et
                                    in vel erat porta.
                                </div>
                            </a>
                        </div>
                        <div class="slider-item">
                            <h2 class="number">2</h2><a>
                                <h1 class="title">Comment travaillons nous?</h1>
                                <div class="desc">We are porttitor amet eleifend sed “Id lorem placerat nulla amet
                                    vivamus dolor” Imperdiet et, tempor aliquam aliquet, magna luctus volutpat nisl et
                                    in vel erat porta.
                                </div>
                            </a>
                        </div>
                        <div class="slider-item">
                            <h2 class="number">3</h2><a>
                                <h1 class="title">Pourquoi nous choisir?</h1>
                                <div class="desc">We are porttitor amet eleifend sed “Id lorem placerat nulla amet
                                    vivamus dolor” Imperdiet et, tempor aliquam aliquet, magna luctus volutpat nisl et
                                    in vel erat porta.
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{--
            <div class="div-box why-choose-us-box mb">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-sm-7">
                            <div class="why-choose-us-box">
                                <h2 class="title-style title-style-1 text-left mb-45"><span class="title-left">Why </span><span class="title-right">Chose US</span></h2>
                                <p class="mb-20">Lorem ipsum dolor sit amet, id nunc suspendisse vitae, reprehenderit ornare bibendum. Suspendisse ac dictum amet mi justo vehicula, cursus velit donec nulla urna mollis, sed sem donec eget ultrices proin, lorem eu vel purus. Dui vehicula morbi lacus et lobortis.</p>
                                <p class="mb-20">Pretium leo est, et dui tincidunt amet. Enim dolor est velit ligula a, et potenti at quis donec aenean, et at molestie neque reiciendis donec. Nibh urna enim vel ridiculus aenean. Sit turpis euismod, ducimus justo tempor, porta ante eget erat. Libero tincidunt in id congue dictumst.</p>
                                <ul class="why-chose-list">
                                    <li> <i class="fa fa-check-circle-o"></i>Expert Operators</li>
                                    <li> <i class="fa fa-check-circle-o"></i>Licensed & Insured</li>
                                    <li> <i class="fa fa-check-circle-o"></i>Excellent Services</li>
                                    <li> <i class="fa fa-check-circle-o"></i>Over 10 Years Experience</li>
                                    <li> <i class="fa fa-check-circle-o"></i>Quality & Reliability</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <figure><img src="{{asset('assets/images/demo/phone.jpg')}}" alt="phone"/></figure>
                        </div>
                    </div>
                </div>
            </div>--}}


@endsection