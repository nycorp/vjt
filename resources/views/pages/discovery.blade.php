@extends('layouts.app')
@section('body-class')
    <body class="project portlio-packery">
    @endsection
    @section('content')
        <div class="div-box">
            <div class="banner-subpage">
                <figure><img src="{{url('assets/images/background/bg-banner-2.jpg')}}" alt="bg-banner"/></figure>
                <div class="banner-subpage-content">
                    <h2>Découverte</h2>
                    <div class="desc">
                        <p>Viens je t'emmene</p>
                        <p>découvrir les coins exotiques</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="div-box mb">
            <div data-js-module="filtering-demo" class="big-demo go-wide">
                <div class="filter-button-group button-group js-radio-button-group container text-center has-border">
                    <button data-filter="*" class="button is-checked">Tous</button>
                    <button data-filter=".bedroom" class="button">Plage</button>
                    <button data-filter=".design" class="button">Hotels</button>
                    <button data-filter=".for" class="button">Jardin</button>
                    <button data-filter=".gift" class="button">Nature</button>
                </div>
                <ul class="grid da-thumbs product-begreen columns-5">
                    <li data-category="bedroom"
                        class="element-item product-item-wrap bedroom design gift space height-380">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/beautiful_nature_landscape_05_hd_picture_166223.jpg')}}"
                                         alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a
                                                href="{{url('posts/beautiful_nature_landscape_05_hd_picture_166223.jpg')}}"
                                                data-rel="prettyPhoto[gallery1]"><i class="fa fa-expand"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="design" class="element-item product-item-wrap design for gift space height-380">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/45110925_1680257195419876_4687163136577372160_n.jpg')}}"
                                         alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a
                                                href="{{url('posts/45110925_1680257195419876_4687163136577372160_n.jpg')}}"
                                                data-rel="prettyPhoto[gallery1]"><i class="fa fa-search"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="for"
                        class="element-item product-item-wrap bedroom gift work height-380 width-40">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/44974708_1680262725419323_3233147953321345024_n.jpg')}}"
                                         alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a
                                                href="{{url('posts/44974708_1680262725419323_3233147953321345024_n.jpg')}}"
                                                data-rel="prettyPhoto[gallery1]"><i class="fa fa-search"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="gift" class="element-item product-item-wrap design gift office height-380">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/45015593_1681606161951646_7890021228180668416_n.jpg')}}"
                                         alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a href="https://vimeo.com/11855056"
                                                                      data-rel="prettyPhoto[gallery2]"><i
                                                    class="fa fa-play-circle"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="for"
                        class="element-item product-item-wrap bedroom gift work height-380 width-40">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/garden_wallpaper_other_nature_1369.jpg')}}" alt="porfolio"/>
                            </figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a
                                                href="{{url('posts/garden_wallpaper_other_nature_1369.jpg')}}"
                                                data-rel="prettyPhoto[gallery1]"><i class="fa fa-search"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="gift" class="element-item product-item-wrap design gift office height-380">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/5558686-pic-of-nature.jpg')}}" alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a href="{{url('posts/5558686-pic-of-nature.jpg')}}"
                                                                      data-rel="prettyPhoto[gallery1]"><i
                                                    class="fa fa-play-circle"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="gift" class="element-item product-item-wrap design gift office height-380">
                        <div class="hover-dir">
                            <figure><img src="{{url('posts/a2y2erW.jpg')}}" alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a href="{{url('posts/a2y2erW.jpg')}}"
                                                                      data-rel="prettyPhoto[gallery1]"><i
                                                    class="fa fa-play-circle"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-category="gift" class="element-item product-item-wrap design gift office height-380">
                        <div class="hover-dir">
                            <figure><img src="{{url('assets/images/demo/project-22.jpg')}}" alt="porfolio"/></figure>
                            <div>
                                <div class="in-slider">
                                    <div class="in-slider-content"><a
                                                href="{{url('assets/images/demo/project-22.jpg')}}"
                                                data-rel="prettyPhoto[gallery1]"><i class="fa fa-play-circle"></i></a><a
                                                href="{{route('post.detail')}}">
                                            <div class="text">Nam libero tempore</div>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <p class="button-project text-center mt-20"><a class="btn btn-13">Load more</a></p>
        </div>
@endsection