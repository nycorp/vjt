@extends('layouts.app')
@section('content')
    <div class="div-box">
        <div class="home-12-slider">
            <div class="recent-news-home_5">
                <div class="recent-news-container">
                    <div id="recent-news-list" class="slick-slider">
                        @foreach($posts as $post)
                            @include('component.slider.content',['post'=>$post])
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="div-box mt">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="blog-wrap">
                        <div class="blog-inner blog-style-grid blog-paging-all blog-col-2">
                            @foreach($posts as $post)
                                @include('component.post.content',['post'=>$post])
                            @endforeach
                        </div>
                    </div>
                    <p class="button-product text-center mt-20"><a href="#" class="btn btn-15">Voir plus</a></p>
                </div>
                @include('component.sidebar.content')
            </div>
        </div>
    </div>

    <div class="div-box">
        <div class="slider-instagram">
            <div data-number="8" data-margin="0" data-loop="no" data-navcontrol="yes" class="begreen-owl-carousel">
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-1.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-2.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-3.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-4.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-5.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-6.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-7.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-8.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-6.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-7.jpg')}}" alt="instagram"/></a>
                </div>
                <div><a href="#"><img src="{{asset('assets/images/demo/instagram-8.jpg')}}" alt="instagram"/></a>
                </div>
            </div>
            <div class="slider-instagram-absolute-1 container">
                <div class="slider-instagram-absolute-1-content">
                    <h3>Instagram.</h3>
                </div>
            </div>
            <div class="slider-instagram-absolute-2 container">
                <div class="slider-instagram-absolute-2-content"><a href="#" class="btn btn-11">@begreen.yolo</a>
                </div>
            </div>
        </div>
    </div>
@endsection