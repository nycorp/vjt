<header class="header yolo-header-style-6">
    <div class="yolo-top-bar">
        <div class="container">
            <div class="row">
                <div class="top-sidebar top-bar-left col-md-4">
                    <aside id="text-11" class="widget widget_text">
                        <div class="textwidget">
                            <div>Bienvenue dans Viens je t'emmene</div>
                        </div>
                    </aside>
                </div>
                <div class="top-sidebar top-bar-right col-md-8">
                    <aside id="text-6" class="widget widget_text">
                        <div class="textwidget"><i class="fa fa-home"></i> Maképé Missoke, Douala <i
                                    style="margin-left: 15px" class="fa fa-phone"></i> 6 94 55 22 75<i
                                    style="margin-left: 15px" class="fa fa-clock-o"></i> Lun - Ven: 09:00 - 21:00
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="col-3 text-left"><a href="#primary-menu"><i class="fa fa-bars"></i></a></div>
        <div class="col-3 text-center">
            <div class="logo">
                <h1><a href="index.html"><img src="{{asset('assets/images/logo/logo.png')}}" alt="logo"/></a></h1>
            </div>
        </div>
        <div class="col-3 text-right">
            <div class="header-right">
                <div class="search-button-wrapper header-customize-item style-default">
                    <div class="icon-search-menu"><i class="wicon fa fa-search"></i></div>
                    <div class="yolo-search-wrapper">
                        <input id="search-ajax" placeholder="Enter keyword to search" type="search"/>
                        <button class="search"><i class="fa fa-search"></i></button>
                        <button class="close"><i class="pe-7s-close"></i></button>
                    </div>
                </div>
                <div class="shopping-cart-wrapper header-customize-item with-price">
                    <div class="widget_shopping_cart_content">
                        <div class="widget_shopping_cart_icon"><i class="wicon fa fa-shopping-bag"></i><span
                                    class="total">0</span></div>
                        <div class="cart_list_wrapper">
                            <div class="scroll-wrapper cart_list product_list_widget scrollbar-inner">
                                <ul class="cart_list product_list_widget scrollbar-inner scroll-content">
                                    <li class="empty">
                                        <h4>An empty cart</h4>
                                        <p>You have no item in your shopping cart</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">

            <div class="main-nav-wrapper">
                <div class="header-logo">
                    <h1><a href="/"><img src="{{asset('assets/images/logo/logo.png')}}" alt="logo"/></a></h1>
                </div>
                <div class="header-left">
                    <nav id="primary-menu" class="main-nav">
                        <ul class="nav">
                            <li class="active menu-item menu-blog">
                                <a href="/">Accueil</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('/')}}">Accueil</a></li>
                                </ul>
                            </li>
                            <li class="mega-menu menu-item">
                                <a href="#">Découverte</a>
                                <ul class="sub-menu sub-menu-mega">
                                    <li class="sub-menu-mega-item active-menu">
                                        <a href="#">Plage exotique</a>
                                        <ul>
                                            <li>
                                                <figure>
                                                    <img src="{{asset('posts/5558686-pic-of-nature.jpg')}}" alt="mega"/>
                                                </figure>
                                                <a href="{{url('discovery')}}" class="name-mega">Easy care
                                                    houseplants</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                            <li>
                                                <figure><img
                                                            src="{{asset('posts/44968928_1680257302086532_6047810476757221376_n.jpg')}}"
                                                            alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">Plaited Planter
                                                    Tutorial Fall For DIY</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                            <li>
                                                <figure><img
                                                            src="{{asset('posts/45015593_1681606161951646_7890021228180668416_n.jpg')}}"
                                                            alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">These Plant Pots Make
                                                    From Upcycled Ceramic</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                            <li>
                                                <figure><img src="{{asset('assets/images/demo/mega-4.jpg')}}"
                                                             alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">Plants Of The Month:
                                                    Sanseivier</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="sub-menu-mega-item"><a href="#">Hotels lune de miel</a>
                                        <ul>
                                            <li>
                                                <figure><img src="{{asset('posts/5558686-pic-of-nature.jpg')}}"
                                                             alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">Easy care
                                                    houseplants</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                            <li>
                                                <figure><img
                                                            src="{{asset('posts/44968928_1680257302086532_6047810476757221376_n.jpg')}}"
                                                            alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">Plaited Planter
                                                    Tutorial Fall For DIY</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                            <li>
                                                <figure><img
                                                            src="{{asset('posts/45015593_1681606161951646_7890021228180668416_n.jpg')}}"
                                                            alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">These Plant Pots Make
                                                    From Upcycled Ceramic</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                            <li>
                                                <figure><img src="{{asset('assets/images/demo/mega-4.jpg')}}"
                                                             alt="mega"/></figure>
                                                <a href="{{url('discovery')}}" class="name-mega">Plants Of The Month:
                                                    Sanseivier</a>
                                                <div class="infomation-mega"><span class="author">Admin</span><span
                                                            class="time">July 2016</span><span class="comment"><i
                                                                class="fa fa-comments-o"></i>0</span></div>
                                            </li>
                                        </ul>
                                    </li>
                                    {{-- <li class="sub-menu-mega-item sale-off-mega-menu"><a href="#">Sale OFF Products</a>
                                         <ul>
                                             <li><a href="#">Jardin secret</a>
                                                 <ul>
                                                     <li><a href="#"><img src="{{asset('assets/images/demo/mega-5.jpg')}}" alt="mg-" width="85" height="100"/></a>
                                                         <p class="mega-right"><span class="product-title">Hay Flowerpot With Saucer Huset</span><span class="price">$80.00–$100.00</span></p>
                                                     </li>
                                                     <li><a href="#"><img src="{{asset('assets/images/demo/mega-6.jpg')}}" alt="mg-" width="85" height="100"/></a>
                                                         <p class="mega-right"><span class="product-title">Cactus Shaped Eraser</span><span class="price">$32.00</span></p>
                                                     </li>
                                                 </ul>
                                             </li>
                                             <li><a href="#">Products</a>
                                                 <ul>
                                                     <li><a href="#"><img src="{{asset('assets/images/demo/mega-7.jpg')}}" alt="mg-" width="85" height="100"/></a>
                                                         <p class="mega-right"><span class="product-title">Hay Flowerpot With Saucer Huset</span><span class="price">$80.00–$100.00</span></p>
                                                     </li>
                                                     <li><a href="#"><img src="{{asset('assets/images/demo/mega-5.jpg')}}" alt="mg-" width="85" height="100"/></a>
                                                         <p class="mega-right"><span class="product-title">Unicorn Glitter Socks</span><span class="price">$40.00</span></p>
                                                     </li>
                                                 </ul>
                                             </li>
                                             <li><a href="#">Products Tags</a>
                                                 <div class="tagcloud"><a href="#" class="tag-link tag-link-position-1">Beautiful</a><a href="#" class="tag-link tag-link-position-2">Cactus</a><a href="#" class="tag-link tag-link-position-3">Featured</a><a href="#" class="tag-link tag-link-position-4">Indoor</a><a href="#" class="tag-link tag-link-position-5">Outdoor</a><a href="#" class="tag-link tag-link-position-6">Tree</a></div>
                                             </li>
                                         </ul>
                                     </li>--}}
                                </ul>
                            </li>
                            <li class="menu-item menu-blog">
                                <a href="#">Contact</a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('contact.us')}}">Contact nous</a></li>
                                    <li><a href="{{route('about.us')}}">Qui nous sommes</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- .header-main-nav-->
                </div>

                <div class="header-customize-item canvas-menu-toggle-wrapper">
                    <div class="canvas-menu-toggle"><i class="fa fa-align-right"></i></div>
                </div>
            </div>

        </div>
    </div>
    <nav class="yolo-canvas-menu-wrapper dark ps-container"><a href="#" class="yolo-canvas-menu-close"><i
                    class="fa fa-close"></i></a>
        <div class="yolo-canvas-menu-inner sidebar">
            <aside id="text-12" class="widget widget_text">
                <div class="textwidget">
                    <div class="begreen-widget ad-spot text-center">
                        <div class="about-image"><img src="{{asset('assets/images/demo/off-menu.png')}}" alt="demo"/>
                        </div>
                        <h2 class="name_author">Be Green</h2>
                        <div class="about-description text-center">I love life and the unique position of being a
                            working artist. I love the thrill of the hunt and am an avid vintage hound, sniffing out the
                            best one-of-a-kinds.
                        </div>
                    </div>
                </div>
            </aside>
            <aside id="text-13" class="widget widget_text">
                <div class="textwidget"><a href="#"><img src="{{asset('assets/images/demo/bn-bs.jpg')}}"
                                                         alt="demo"/></a></div>
            </aside>
            <aside id="yolo-social-profile-6" class="widget widget-social-profile">
                <ul class="social-profile social-icon-bordered">
                    <li><a title="Facebook" href="#" target="_blank"><i class="fa fa-facebook"></i>Facebook</a></li>
                    <li><a title="Twitter" href="#" target="_blank"><i class="fa fa-twitter"></i>Twitter</a></li>
                    <li><a title="Skype" href="#" target="_blank"><i class="fa fa-skype"></i>Skype</a></li>
                    <li><a title="Youtube" href="#" target="_blank"><i class="fa fa-youtube"></i>Youtube</a></li>
                </ul>
            </aside>
        </div>
    </nav>
</header>