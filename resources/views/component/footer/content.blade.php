<div class="div-box">
    {{--<footer id="yolo-footer-wrapper">
        <div class="yolo-footer-wrapper footer-2">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="footer-col footer-2-col-1">
                                <h3>Contact</h3>
                                <div class="icon-info">
                                    <p><strong>Address</strong><br/>Maképé Missoke, Douala.</p><i class="fa fa-map-marker"></i>
                                </div>
                                <div class="icon-info">
                                    <p><strong>Appelez nous</strong><br/>
                                        6 94 55 22 75</p><i class="fa fa-phone"></i>
                                </div>
                                <div class="icon-info">
                                    <p><strong>Email Us</strong><br/><a href="../../cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="d881b7b4b798abada8a8b7aaacf6bbb7b5">[email&#160;protected]</a></p><i class="fa fa-envelope"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="footer-col footer-2-col-2">
                                <h2><a href="index.html" class="logo"><img src="{{asset('assets/images/logo/logo.png')}}" alt="logo1" width="84" height="63" class="vc_single_image-img attachment-full"/></a></h2>
                                <p><strong>Nous travaillons ainsi</strong></p>
                                <p><strong>Lun - Ven:</strong></p>
                                <p>09:00 - 21:00</p>
                                <p><strong>Sam:</strong></p>
                                <p>09:00 - 12:00</p>
                                <p><strong>Dim:</strong></p>
                                <p>Fermé</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="footer-col footer-2-col-3">
                                <h3>Instagram</h3>
                                <ul class="instagram-pics instagram-size-thumbnail col-4">
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-1.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-2.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-3.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-4.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-5.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-6.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-7.jpg')}}" alt="instagram"/></a></li>
                                    <li><a href="#"><img src="{{asset('assets/images/demo/instagram-8.jpg')}}" alt="instagram"/></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="click-back-top-footer">
                    <button type="button" class="sn-btn sn-btn-style-17 sn-back-to-top fixed-right-bottom">
                        <i class="fa fa-angle-up"></i>
                    </button>
                </div>
                <div class="container">
                    <p class="copyright">VIENS JE T'EMMENE © 2018 COPYRIGHT. MADE BY <b><a href="http://nycorp.rf.gd" target="_blank">N-Y Corp</a></b></p>
                </div>
            </div>
        </div>
    </footer>--}}

    <footer id="yolo-footer-wrapper">
        <div class="yolo-footer-wrapper footer-1">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-col footer-1-col-1">
                                <h2><a href="index.html" class="logo"><img
                                                src="{{asset('assets/images/logo/logo-footer.png')}}" alt="logo1"
                                                width="84" height="63" class="vc_single_image-img attachment-full"/></a>
                                </h2>
                                <p>We are a creative company that specializes in strategy & design. We like to create
                                    things with like – minded people who are serious about their passions.</p>
                                <ul id="social-footer">
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-col footer-1-col-2">
                                <h3>Notre bureau</h3>
                                <div class="icon-info">
                                    <p><strong>Adresse</strong><br/>Maképé Missoke, Douala.</p><i
                                            class="fa fa-map-marker"></i>
                                </div>
                                <div class="icon-info">
                                    <p><strong>Appelez nous</strong><br/>(237) 6 94 55 22 75</p><i
                                            class="fa fa-phone"></i>
                                </div>
                                <div class="icon-info">
                                    <p><strong>Email Us</strong><br/><a
                                                href="http://demo.yolotheme.com/cdn-cgi/l/email-protection"
                                                class="__cf_email__" data-cfemail="c3a1a6a4b1a6a6ad83baacafaceda0acae">[email&#160;protected]</a>
                                    </p><i class="fa fa-envelope"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-col footer-1-col-3">
                                <h3>News Letter</h3>
                                <p>Subcribe to our newsletter to get more free tips. No Spam, Promise.</p>
                                <form>
                                    <input type="text" placeholder="Enter your mail"/><a href="#"
                                                                                         class="btn">S'abonner</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="click-back-top-footer">
                        <button type="button" class="sn-btn sn-btn-style-17 sn-back-to-top fixed-right-bottom"><i
                                    class="fa fa-angle-up"></i>top
                        </button>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <p class="copyright">VIENS JE T'EMMENE © 2018 COPYRIGHT. MADE BY <b><a href="http://nycorp.rf.gd"
                                                                                           target="_blank">N-Y Corp</a></b>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>