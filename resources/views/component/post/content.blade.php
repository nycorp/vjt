<article class="post">
    <div class="post-item">
        <div class="entry-wrap">
            <div class="entry-thumbnail-wrap">
                <div data-number="1" data-margin="10" data-loop="yes"
                     data-navcontrol="yes" class="begreen-owl-carousel">
                    @foreach($post->files as $file)
                        <div class="entry-thumbnail">
                            <a href="#" class="entry-thumbnail_overlay">
                                <img src="{{asset($file->type==='url'?$post->files->where('type','picture')->first()->path:$file->path)}}"
                                     alt="blog" width="420" height="280"
                                     class="img-responsive"/>
                            </a>
                            <a href="{{asset($file->path)}}"
                               data-rel="prettyPhoto[gallery3]" class="prettyPhoto">
                                <i class="fa fa-arrows-alt"></i></a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="entry-content-wrap">
                <div class="entry-detail">
                    <h3 class="entry-title">
                        <a href="{{url('post-detail')}}">{{$post->title}}</a>
                    </h3>
                    <div class="entry-post-meta-wrap">
                        <ul class="entry-meta">
                            <li class="entry-meta-author">
                                <i class="fa fa-pencil-square-o p-color"></i>
                                <a href="#">{{$post->user->name}}</a>
                            </li>
                            <li class="entry-meta-date">
                                <i class="fa fa-clock-o p-color"></i>
                                <a href="#">{{$post->updated_at->format('d M Y') }} </a>
                            </li>
                            <li class="entry-meta-category">
                                <i class="fa fa-folder-open p-color"></i>
                                <a href="#">Exotique</a>, <a href="#">Tree</a>
                            </li>
                            <li class="entry-meta-comment">
                                <a href="#"><i class="fa fa-comments-o p-color"></i>{{$post->comments->count()}}
                                    Commentaires</a>
                            </li>
                        </ul>
                    </div>
                    <div class="entry-excerpt">
                        <p>{{str_limit($post->description)}}</p>
                    </div>
                    <div class="entry-meta-tag">
                        <label><i class="fa fa-tags"></i>Tags :</label>
                        <a href="#">Plant Care</a>
                        <a href="#">viens je t'emmene</a>
                    </div>
                    <a href="{{url('post-detail')}}" class="btn-readmore">
                        <span class="span-text">Lire</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</article>