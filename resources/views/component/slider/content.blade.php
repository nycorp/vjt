<div class="recent-news-item">
    <div class="post-thumbnail">
        <a href="{{url('post-detail',$post)}}">
            <img src="{{asset($post->files()->first()->type==='url'?$post->files->where('type','picture')->first()->path:$post->files()->first()->path)}}"
                 alt="{{$post->title}}" width="865"
                 height="585" class="attachment-large size-large wp-post-image"/>
        </a>
    </div>
    <div class="post-information">
        <div class="category"><a href="#">{{$post->category->label}}</a></div>
        <h3 class="post-title"><a href="{{url('post-detail',$post)}}">{{$post->title}}</a></h3>
        <div class="post-meta"><span class="post-date"><i
                        class="fa fa-calendar"></i>{{$post->updated_at->format('d M Y')}}</span>
            <span class="post-count-comments"><i class="fa fa-comments"></i>{{$post->comments->count()}}
                Commentaires</span>
        </div>
        <a href="{{url('post-detail',$post)}}" class="btn-readmore">
            <span class="span-text">Lire d'avantage</span>
        </a>
    </div>
</div>