<div class="col-md-3">
    <div class="sidebar sidebar-product sidebar-blog">
        {{--<aside class="mb-45 img-sidebar text-center">--}}
        {{--<figure><img src="{{asset('assets/images/demo/about-us.png')}}" alt="about-us"/></figure>--}}
        {{--<p>Sara Gabrena I love life and the unique position of being a working artist. I love--}}
        {{--the thrill of the hunt and am an avid vintage hound, sniffing out the best--}}
        {{--one-of-a-kinds.</p>--}}
        {{--</aside>--}}
        <aside class="mb-45 search-sidebar">
            <h2 class="text-center mb-20">Search</h2>
            <form class="form-input">
                <input type="text" placeholder="Search here..."/><a href="#"><i
                            class="fa fa-search"></i></a>
            </form>
        </aside>
        <aside class="mb-45 categories-blog">
            <h2 class="text-center mb-20">Categories</h2>
            <ul class="cat-list-blog">
                <li><a href="#"> <i class="fa fa-chevron-right"></i>Plage</a></li>
                <li><a href="#"> <i class="fa fa-chevron-right"></i>Hotel</a></li>
                <li><a href="#"> <i class="fa fa-chevron-right"></i>Safari</a></li>
            </ul>
        </aside>
        <aside class="mb-45">
            <ul class="posts-thumbnail-list size-thumbnail">
                <li class="clearfix">
                    <div class="posts-thumbnail-image"><a href="#"><img
                                    src="{{asset('assets/images/demo/blog1.jpg')}}" alt="blog"
                                    width="865" height="585"/></a></div>
                    <div class="posts-thumbnail-content">
                        <h4 class="p-font"><a href="#">Choose the plants and pots</a></h4>
                        <div class="posts-thumbnail-meta"><span class="author vcard">admin</span>
                            <time datetime="2016-07-15T08:11:58+00:00">Jul, 2016</time>
                            <span class="comment-count"><i class="fa fa-comments-o"></i><a
                                        href="#">0</a></span>
                        </div>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="posts-thumbnail-image"><a href="#"><img
                                    src="{{asset('assets/images/demo/blog3.jpg')}}" alt="blog"
                                    width="865" height="585"/></a></div>
                    <div class="posts-thumbnail-content">
                        <h4 class="p-font"><a href="#">Lina Skukauke Shopify</a></h4>
                        <div class="posts-thumbnail-meta"><span class="author vcard">admin</span>
                            <time datetime="2016-07-15T08:09:32+00:00">Jul, 2016</time>
                            <span class="comment-count"><i class="fa fa-comments-o"></i><a
                                        href="#">0</a></span>
                        </div>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="posts-thumbnail-image"><a href="#"><img
                                    src="{{asset('assets/images/demo/blog6.jpg')}}" alt="blog"
                                    height="585"/></a></div>
                    <div class="posts-thumbnail-content">
                        <h4 class="p-font"><a href="#">Connie Saxe Exotique</a></h4>
                        <div class="posts-thumbnail-meta"><span class="author vcard">admin</span>
                            <time datetime="2016-07-15T08:08:26+00:00">Jul, 2016</time>
                            <span class="comment-count"><i class="fa fa-comments-o"></i><a
                                        href="#">0</a></span>
                        </div>
                    </div>
                </li>
            </ul>
        </aside>
        <aside class="mb-45 follow-me">
            <h2 class="text-center mb-20">Suivez moi</h2>
            <div class="desc-follow mb-20">Je vous emmènent</div>
            <ul class="social-profile text-center">
                <li><a title="Facebook" href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a title="Twitter" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a title="Pinterest" href="#" target="_blank"><i
                                class="fa fa-pinterest"></i></a></li>
                <li><a title="GooglePlus" href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                </li>
                <li><a title="Vimeo" href="#" target="_blank"><i class="fa fa-vimeo-square"></i></a>
                </li>
                <li><a title="Instagram" href="#" target="_blank"><i
                                class="fa fa-instagram"></i></a></li>
                <li><a title="Linkedin" href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                </li>
                <li><a title="Rss" href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                <li><a title="Youtube" href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                </li>
            </ul>
        </aside>
        <aside class="mb-45 ad-pot">
            <h2 class="text-center mb-20">Ad Spot</h2>
            <figure><img src="{{asset('assets/images/demo/ad.jpg')}}" alt="ad"/></figure>
        </aside>
    </div>
</div>