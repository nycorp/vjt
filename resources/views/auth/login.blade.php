@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email"
                                       class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-7-contact-me">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="home-6-contact-me-form">
                        <h3>How To Contact Me</h3>
                        <p>Lorem ipsum dolor sit amet, vel inventore lacus sapien ullamcorper. Suscipit ut nulla purus,
                            erat mattis nisl tenti pellentesque ridiculus. </p>
                        <form>
                            <p>
                                <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}"
                                       class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </p>
                            <p>
                                <input id="password" type="password" placeholder="{{ __('Password') }}"
                                       class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                                       required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </p>
                            <p>
                                <input type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>

                            </p>
                            <p><a href="#" class="btn">Send Now</a></p>
                        </form>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="recent-news-home_3">
                        <div class="recent-news-container">
                            <div data-number="1" data-margin="0" data-loop="yes" data-navcontrol="yes"
                                 class="recent-news-list begreen-owl-carousel owl-carousel owl-loaded owl-drag">


                                <div class="owl-stage-outer">
                                    <div class="owl-stage"
                                         style="transform: translate3d(-2160px, 0px, 0px); transition: all 0s ease 0s; width: 4320px;">
                                        <div class="owl-item cloned" style="width: 720px;">
                                            <div class="recent-news-item">
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 720px;">
                                            <div class="recent-news-item">
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 720px;">
                                            <div class="recent-news-item">
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item active active-first active-last" style="width: 720px;">
                                            <div class="recent-news-item">
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 720px;">
                                            <div class="recent-news-item">
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 720px;">
                                            <div class="recent-news-item">
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="row">
                                                    <article>
                                                        <div class="post-thumbnail"><img
                                                                    src="images/demo/blog6-300x203.jpg" alt="blog6"
                                                                    width="300" height="203"
                                                                    class="attachment-medium size-medium wp-post-image"><a
                                                                    href="#">
                                                                <div class="post-meta">
                                                                    <div class="post-meta-inner"><span class="post-day">15 </span><span
                                                                                class="post-month">July </span></div>
                                                                </div>
                                                            </a></div>
                                                        <div class="post-information">
                                                            <div class="category"><a href="#">Planter</a> / <a href="#">Tree</a>
                                                            </div>
                                                            <h3 class="post-blog-title"><a href="#">Choose the plants
                                                                    and pots</a></h3>
                                                            <p class="post-blog-excerpt">Lorem ipsum dolor sit amet,
                                                                consectetur adipiscing elit, sed do eiusmod tempor
                                                                incididunt ut labore et dolore magna aliqua. Ut</p><a
                                                                    href="#" class="btn-readmore"><span
                                                                        class="span-text">Read post</span></a>
                                                            <div class="polygon"></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-nav">
                                    <div class="owl-prev"><span class="fa fa-angle-left"></span></div>
                                    <div class="owl-next"><span class="fa fa-angle-right"></span></div>
                                </div>
                                <div class="owl-dots disabled"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
