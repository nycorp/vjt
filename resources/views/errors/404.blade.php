@extends('layouts.app')
@section('title')
    404
@endsection
@section('body-class')
    <body class="page page-404">
    @endsection
    @section('content')
        <div class="div-box">
            <figure class="img-404"><img src="{{asset('assets/images/demo/page-404.jpg')}}" alt=""/>
                <a href="{{url('welcome')}}" class="return-home">Retourner sur la page d'accueil</a>
            </figure>
        </div>
@endsection